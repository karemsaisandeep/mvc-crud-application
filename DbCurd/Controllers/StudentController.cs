﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DbCurd.Models;
using MySql.Data.MySqlClient;

namespace DbCurd.Controllers
{
    public class StudentController : Controller
    {
        string connectionString = @"Server=localhost;Database=sandeep;Uid=root;Pwd=8653";
        // GET: Student
        [HttpGet]
        public ActionResult Index()
        {
            DataTable dtblStudent = new DataTable();
            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                con.Open();
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter("select * from student",con);
                dataAdapter.Fill(dtblStudent);
            }
            return View(dtblStudent);
        }

       
        [HttpGet]
        // GET: Student/Create
        public ActionResult Create()
        {
            return View(new StudentModel());
        }

        // POST: Student/Create
        [HttpPost]
        public ActionResult Create(StudentModel studentModel)
        {
            using(MySqlConnection con = new MySqlConnection(connectionString))
            {
                con.Open();
                string query = "Insert into student values(@studentId,@firstName,@lastName,@age)";
                MySqlCommand cmd = new MySqlCommand(query, con);
                cmd.Parameters.AddWithValue("@studentId", studentModel.studentId);
                cmd.Parameters.AddWithValue("@firstName", studentModel.firstName);
                cmd.Parameters.AddWithValue("@lastName", studentModel.lastName);
                cmd.Parameters.AddWithValue("@age", studentModel.age);
                cmd.ExecuteNonQuery();
            }
            return RedirectToAction("Index");
        }

        // GET: Student/Edit/5
        public ActionResult Edit(int id)
        {
            StudentModel studentModel = new StudentModel();
            DataTable dataTable = new DataTable();
            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                con.Open();
                string query = "select * from student where studentId = @studentId";
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter(query, con);
                dataAdapter.SelectCommand.Parameters.AddWithValue("@studentId", id);
                dataAdapter.Fill(dataTable);
            }
            if(dataTable.Rows.Count == 1)
            {
                studentModel.studentId = Convert.ToInt32(dataTable.Rows[0][0].ToString());
                studentModel.firstName = dataTable.Rows[0][1].ToString();
                studentModel.lastName = dataTable.Rows[0][2].ToString();
                studentModel.age = Convert.ToInt32(dataTable.Rows[0][3].ToString());
                return View(studentModel);
            }
            else
            return RedirectToAction("Index");
        }

        // POST: Student/Edit/5
        [HttpPost]
        public ActionResult Edit(StudentModel studentModel)
        {
            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                con.Open();
                string query = "update student set firstName = @firstname, lastName = @lastname, age = @age where studentId = @studentId";
                MySqlCommand cmd = new MySqlCommand(query, con);
                cmd.Parameters.AddWithValue("@studentId", studentModel.studentId);
                cmd.Parameters.AddWithValue("@firstName", studentModel.firstName);
                cmd.Parameters.AddWithValue("@lastName", studentModel.lastName);
                cmd.Parameters.AddWithValue("@age", studentModel.age);
                cmd.ExecuteNonQuery();
            }
            return RedirectToAction("Index");
        }

        // GET: Student/Delete/5
        public ActionResult Delete(int id)
        {
            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                con.Open();
                string query = "delete from student where studentId = @studentId";
                MySqlCommand cmd = new MySqlCommand(query, con);
                cmd.Parameters.AddWithValue("@studentId",id);
              
                cmd.ExecuteNonQuery();
            }
            return RedirectToAction("Index");
        }

        
    }
}
